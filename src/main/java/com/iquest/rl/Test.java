package com.iquest.rl;

/**
 * This is the main class.
 *
 * @author Laura Mamina
 */
public class Test {

  public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
    MyClass myClass = (MyClass) Factory.getMyClassInMyClassLoader();
  }
}
