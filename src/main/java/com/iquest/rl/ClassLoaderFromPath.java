package com.iquest.rl;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * This is a simple class loader;
 *
 * @author Laura Mamina
 */
public class ClassLoaderFromPath extends ClassLoader {

  public Class<?> loadClass(String name, String path) throws ClassNotFoundException {
    if (findLoadedClass(name) != null) {
      return super.loadClass(name);
    }

    byte[] classData = readClassByteCode(path);
    return defineClass(name, classData, 0, classData.length);
  }

  private byte[] readClassByteCode(String filePath) throws ClassNotFoundException {
    try (FileInputStream inStream = new FileInputStream(filePath)) {
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();

      int data = inStream.read();
      while (data != -1) {
        buffer.write(data);
        data = inStream.read();
      }

      return buffer.toByteArray();
    } catch (IOException e) {
      throw new ClassNotFoundException("The class file " + filePath + " not found.", e);
    }
  }
}