package com.iquest.rl;

/**
 * @author Laura Mamina
 */
public class MyClass {

  /**
   * This method returns a message which identifies the class.
   */
  public String getMessage() {
    return "This is MyClass";
  }
}
