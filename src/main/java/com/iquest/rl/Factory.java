package com.iquest.rl;

/**
 * @author Laura Mamina
 */
public class Factory {

  private static String myClassPath = "d:/Work/WorkspaceIDEA/classloader/src/main/resources/com/iquest/rl/MyClass.class";
  private static String myClassName = "com.iquest.rl.MyClass";
  private static ClassLoaderFromPath classLoader = new ClassLoaderFromPath();

  public static MyClass getMyClassInCurrentLoader() {
    return new MyClass();
  }

  public static MyClass getMyClassInMyClassLoader() throws ClassNotFoundException, InstantiationException,
      IllegalAccessException {
    Class<?> myObjectClass = classLoader.loadClass(myClassName, myClassPath);
    MyClass object = (MyClass) myObjectClass.newInstance();
    return object;
  }
}
